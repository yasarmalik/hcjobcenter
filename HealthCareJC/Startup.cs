﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HealthCareJC.Startup))]
namespace HealthCareJC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
