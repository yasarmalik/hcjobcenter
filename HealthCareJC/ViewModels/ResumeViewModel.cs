﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.ViewModels
{
    public class ResumeViewModel
    {
        public JobSeeker jobseeker { get; set; }
        public JobSeekerEducation jobseekereducation { get; set; }

        public JobSeekerExperience jobseekerexperience { get; set; }
        public List<JobSeeker> Listjobseeker { get; set; }

        public IEnumerable<SelectListItem> JobType { get; set; }

        public List<JobSeekerExperience> ListjobseekerExperience { get; set; }
        public List<JobSeekerEducation> ListjobseekerEducation { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }


    }
}