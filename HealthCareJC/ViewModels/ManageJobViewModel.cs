﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class ManageJobViewModel
    {
        public Job jobs { get; set; }

        public List<Job> jobList { get; set; }

        public Dictionary<int,string> count { get; set; }

    }
    
}