﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HealthCareJC.Models;


namespace HealthCareJC.ViewModels
{
    public class RolePermissionViewModel
    {
        public Permission Permission { get; set; }
        public RolePermission RolePermission { get; set; }

        
    }
}