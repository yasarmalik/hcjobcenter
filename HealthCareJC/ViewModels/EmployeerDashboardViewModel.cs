﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class EmployeerDashboardViewModel
    {

        public List<Employer> EmployerList { get; set; }
        public List<ApplicationUser>user { get; set; }
        public IEnumerable<dynamic> emplist { get; set; }


    }
}