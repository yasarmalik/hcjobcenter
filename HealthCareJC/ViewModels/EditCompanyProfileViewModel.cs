﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class EditCompanyProfileViewModel
    {
        public Employer employer{ get; set; }
        public ApplicationUser user { get; set; }
    }
}