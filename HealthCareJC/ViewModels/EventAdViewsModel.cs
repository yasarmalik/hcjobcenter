﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.ViewModels
{
    public class EventAdViewsModel
    {
        public EventAd eventad { get; set; }

        public List<Job> jobList{get;set;}
        public IEnumerable<dynamic> eventlist { get; set; }

        public List<EventAd> eventAdList { get; set; }
        public ApplicationUser user { get; set; }
        [DataType(DataType.EmailAddress)]
        [Remote("CheckExistingEmailUser", "EventAds", ErrorMessage = "Email already exists! Please sign in to post your event")]
        public string UserEmail { get; set; }
        [Remote("CheckEventCoupon", "ManageCoupons", ErrorMessage = "Invalid Coupon.")]
        public string CouponName { get; set; }
        public string payment { get; set; }
        public float EventRate { get; set; }
    }
}