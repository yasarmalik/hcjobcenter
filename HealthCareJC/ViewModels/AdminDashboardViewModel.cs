﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class AdminDashboardViewModel
    {
        public IEnumerable<ApplicationUser> user { get; set; }
    }
}