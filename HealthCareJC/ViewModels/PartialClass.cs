﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class PartialClass
    {
        [MetadataType(typeof(Job))]
        public partial class Job
        {
            public string count { get; set; }

        }
    }
  
}