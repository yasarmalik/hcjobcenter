﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class ResumeDetailViewModel
    {
        public List<JobSeeker> jobseekerList { get; set; }
        public List<JobSeekerEducation> jobseekerEducationList { get; set; }

        public List<string> skills { get; set; }
    }
}