﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class JobseekerDashboardViewModel
    {
        public List<JobSeeker> jobseekerList { get; set; }
        public List<string> skills { get; set; }
        //public List<Job> jobList { get; set; }
        public dynamic jobList { get; set; }
    }
}