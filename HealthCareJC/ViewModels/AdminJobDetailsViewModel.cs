﻿
using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.ViewModels
{

    public class AdminJobDetailsViewModel
    {
       
        public Job jobs { get; set; }
        public EventAd eventAd { get; set; }
        public ApplicationUser user { get; set; }
        public Employer employer { get; set; }
      
        [DataType(DataType.EmailAddress)]
      //  [Remote("CheckExistingEmailCompany", "Jobs", ErrorMessage = "Email already exists! Please sign in to post a job")]
        public string CompanyEmail { get; set; }


       
        [DataType(DataType.EmailAddress)]
        [Remote("CheckExistingEmailUser", "Jobs", ErrorMessage = "Email already exists. Please sign in to post a job.")]
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }
        public IEnumerable<SelectListItem> JobCatagory { get; set; }
        public IEnumerable<SelectListItem> EducationLevel { get; set; }
        public IEnumerable<SelectListItem> HealthProfessionalType { get; set; }
        public IEnumerable<SelectListItem> Specility { get; set; }

        public IEnumerable<SelectListItem> JobType { get; set; }
        public IEnumerable<SelectListItem> CompensationType { get; set; }
        public IEnumerable<SelectListItem> ExperienceLevel { get; set; }
        public IEnumerable<SelectListItem> JobStatus { get; set; }


        public List<Job> jobList { get; set; }
        public List<EventAd> eventAdList { get; set; }

    }
}