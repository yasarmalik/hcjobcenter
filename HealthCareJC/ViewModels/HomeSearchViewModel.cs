﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.ViewModels
{
    public class HomeSearchViewModel
    {
        public int category { get; set; }

        public string location { get; set; }

        public int specialityid { get; set; }
        public string MdNpPa { get; set; }
        public string NursesMedAsst { get; set; }
        public string TechsIt { get; set; }

        public string AdminStaff { get; set; }
        public IEnumerable<SelectListItem> Specility { get; set; }

        public IEnumerable<dynamic> jobs { get; set; }
        public List<JobCategory> jobCategory { get; set; }
        public decimal? JobAmount { get; set; }


    }
}