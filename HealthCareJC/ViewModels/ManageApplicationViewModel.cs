﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class ManageApplicationViewModel
    {
        public List<JobSeeker> jobseekerList { get; set; }
    }
}