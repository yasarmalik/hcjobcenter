﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.ViewModels
{
    public class JobDetailViewModel
    {
        public JobSeeker jobseeker { get; set; }
        public IEnumerable<dynamic> joblist { get; set; }
        
    }
}