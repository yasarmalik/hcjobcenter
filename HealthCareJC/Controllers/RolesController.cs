﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthCareJC;
using HealthCareJC.Models;
using HealthCareJC.ViewModels;

namespace HealthCareJC.Controllers
{
    using System.Net;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using HealthCareJC;
    using HealthCareJC.Models;

    public class RolesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public RolesController()
        {
           
        }
    private ApplicationUserManager _userManager;
    public ApplicationUserManager UserManager
    {
      get
      {
        return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
      }
      set
      {
        _userManager = value;
      }
    }

    private ApplicationRoleManager _roleManager;
    public ApplicationRoleManager RoleManager
    {
      get
      {
        return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
      }
      private set
      {
        _roleManager = value;
      }
    }

    public ActionResult Index()
    {
      return View(RoleManager.Roles);
    }

    //
    // GET: /Roles/Create
    public ActionResult Create()
    {
      return View();
    }

    //
    // POST: /Roles/Create
    [HttpPost]
    public async Task<ActionResult> Create(RoleViewModel roleViewModel)
    {
      if (ModelState.IsValid)
      {
        var role = new ApplicationRole(roleViewModel.Name);

        var roleresult = await this.RoleManager.CreateAsync(role);
        if (!roleresult.Succeeded)
        {
          ModelState.AddModelError(string.Empty, roleresult.Errors.First());
          return View();
        }
        return RedirectToAction("Index");
      }

      return View();
    }

    public async Task<ActionResult> Edit(string id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      var role = await RoleManager.FindByIdAsync(id);
      if (role == null)
      {
        return HttpNotFound();
      }
      RoleViewModel roleModel = new RoleViewModel { Id = role.Id, Name = role.Name };

      // Update the new Description property for the ViewModel:
      return View(roleModel);
    }


    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit([Bind(Include = "Name,Id,Description")] RoleViewModel roleModel)
    {
      if (ModelState.IsValid)
      {
        var role = await RoleManager.FindByIdAsync(roleModel.Id);
        role.Name = roleModel.Name;

        // Update the new Description property:
        await RoleManager.UpdateAsync(role);
        return RedirectToAction("Index");
      }
      return View();
    }

  }

  // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
  public class ApplicationRoleManager : RoleManager<ApplicationRole>
  {
    public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
      : base(roleStore)
    {
    }

    public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
    {
      return new ApplicationRoleManager(new RoleStore<ApplicationRole>(context.Get<ApplicationDbContext>()));
    }
  }

      public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        
    }
}