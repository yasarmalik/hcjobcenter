﻿using HealthCareJC.Services;
using HealthCareJC.ViewModels;
using HealthCareJC.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Controllers
{
    public class ResumesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private JobServices jobServices = new JobServices();
        // GET: Resumes
        public ActionResult AddResumes()
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
                var usr = db.Users.Find(usrid);
                var model = new ResumeViewModel
                {
                    JobType = jobServices.JobTypeData(),
                    Name = usr.FirstName + " " + usr.LastName,
                    Email = usr.Email,
                    PhoneNo = usr.PhoneNumber,
                    ListjobseekerEducation = db.JobSeekerEducations.Where(u => u.UserId == usr.Id).ToList(),
                    ListjobseekerExperience = db.JobSeekerExperiences.Where(u => u.UserId == usr.Id).ToList(),


                };
                return View(model);
            }
            return RedirectToAction("ClearLogin", "Account");


        }





        public ActionResult EditResume(string UserID)
        {
           
              
                var model = new EditResumeViewModel
                {

                    jobseeker = db.Jobseekers.Where(u => u.UserId == UserID).FirstOrDefault(),
                    JobType = jobServices.JobTypeData(),
                    ListjobseekerEducation = db.JobSeekerEducations.Where(u => u.UserId == UserID).ToList(),
                    ListjobseekerExperience = db.JobSeekerExperiences.Where(u => u.UserId == UserID).ToList(),


                };


                var obj = db.Users.Where(u => u.Id == UserID).SingleOrDefault();

                model.fName = obj.FirstName;
                model.Lname= obj.LastName;
                model.Email = obj.Email;
                model.PhoneNo = obj.PhoneNumber;
                return View(model);

            



        }

        public ActionResult SaveEditResume(EditResumeViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
                var usr = db.Users.Find(usrid);
                try
                {
                    JobSeeker jobseeker = db.Jobseekers.Where(u => u.UserId == usrid).FirstOrDefault();
                    jobseeker.ProfessionalTitle = vm.jobseeker.ProfessionalTitle;
                    jobseeker.Skills = vm.jobseeker.Skills;



                    jobseeker.CurrentAddress = vm.jobseeker.CurrentAddress;
                    
                    string fileName = "";
                    string path = "";
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0];

                        if (file != null && file.ContentLength > 0)
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/JobseekerPhotos"), fileName);
                            file.SaveAs(path);
                            jobseeker.PicturePath = fileName;

                        }
                        else if (!string.IsNullOrEmpty(vm.jobseeker.PicturePath))
                        {
                            jobseeker.PicturePath = vm.jobseeker.PicturePath;

                        }
                    }
                    else
                    {

                        jobseeker.PicturePath = "avatar-placeholder.png";

                    }
                    

                    




                    db.Jobseekers.Attach(jobseeker);
                    db.Entry(jobseeker).State = EntityState.Modified;
                    db.SaveChanges();
                    var obj = db.Users.Where(u => u.Id == usr.Id).SingleOrDefault();
                    if (obj != null)
                    {
                        obj.FirstName = vm.fName;
                        obj.LastName = vm.Lname;
                        obj.PhoneNumber = vm.PhoneNo;
                        db.Users.Attach(obj);
                        db.Entry(obj).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                }
                catch (Exception)
                {

                    throw;
                }

                return RedirectToAction("JobSeekerDashboard", "Dashboards");
            }
            return RedirectToAction("ClearLogin", "Account");
        }


        public ActionResult SaveEducation(EditResumeViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    JobSeekerEducation jobseekereducation = new JobSeekerEducation();
                    var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
                    var usr = db.Users.Find(usrid);
                    jobseekereducation.UserId = usr.Id;
                    jobseekereducation.School = vm.jobseekereducation.School;
                    jobseekereducation.Degree = vm.jobseekereducation.Degree;
                    jobseekereducation.StartDate = vm.jobseekereducation.StartDate;
                    if (vm.jobseekereducation.Continue)
                    {
                        jobseekereducation.Continue = vm.jobseekereducation.Continue;
                    }
                    else
                    {
                        jobseekereducation.EndDate = vm.jobseekereducation.EndDate;
                    }
                    jobseekereducation.Percentage = vm.jobseekereducation.Percentage;

                    db.JobSeekerEducations.Add(jobseekereducation);
                    db.SaveChanges();
                    var obj = db.JobSeekerEducations.Where(u => u.UserId == usr.Id).ToList();

                    return PartialView(@"PartialViews\_EducationList", obj);



                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return RedirectToAction("ClearLogin", "Account");



        }
        public ActionResult SaveExperience(EditResumeViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                JobSeekerExperience jobseekerexperience = new JobSeekerExperience();
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
                var usr = db.Users.Find(usrid);
                jobseekerexperience.UserId = usr.Id;
                jobseekerexperience.JobTitle = vm.jobseekerexperience.JobTitle;
                jobseekerexperience.CompanyName = vm.jobseekerexperience.CompanyName;
                jobseekerexperience.JobType = vm.jobseekerexperience.JobType;
                jobseekerexperience.StartDate = vm.jobseekerexperience.StartDate;
                if (jobseekerexperience.Continue)
                {
                    jobseekerexperience.Continue = vm.jobseekerexperience.Continue;
                }
                else
                {
                    jobseekerexperience.EndDate = vm.jobseekerexperience.EndDate;

                }
                jobseekerexperience.Responsiblities = vm.jobseekerexperience.Responsiblities;

                db.JobSeekerExperiences.Add(jobseekerexperience);
                db.SaveChanges();
                var obj = db.JobSeekerExperiences.Where(u => u.UserId == usr.Id).ToList();

                return PartialView(@"PartialViews\_ExperienceList", obj);
            }
            return RedirectToAction("ClearLogin", "Account");


        }



        public ActionResult AddSaveResume(ResumeViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
                var usr = db.Users.Find(usrid);
                try
                {
                    JobSeeker jobseeker = new JobSeeker();
                    jobseeker.ProfessionalTitle = vm.jobseeker.ProfessionalTitle;
                    jobseeker.Skills = vm.jobseeker.Skills;



                    jobseeker.CurrentAddress = vm.jobseeker.CurrentAddress;
                    
                    string fileName = "";
                    string path = "";
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0];

                        if (file != null && file.ContentLength > 0)
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/JobseekerPhotos"), fileName);
                            file.SaveAs(path);
                        }

                        jobseeker.PicturePath = fileName;
                    }
                    else
                    {
                        jobseeker.PicturePath = "avatar-placeholder.png";

                    }




                    db.Jobseekers.Add(jobseeker);
                    db.SaveChanges();
                    var obj = db.Users.Where(u => u.Id == usr.Id).SingleOrDefault();
                    if (obj != null)
                    {
                        
                        obj.PhoneNumber = vm.PhoneNo;
                        db.Users.Attach(obj);
                        db.Entry(obj).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                }
                catch (Exception)
                {

                    throw;
                }

                return RedirectToAction("JobSeekerDashboard", "Dashboards");
            }
            return RedirectToAction("ClearLogin", "Account");

        }



        public ActionResult ResumeDetail(string id)
        {
            var model = new ResumeDetailViewModel {
                jobseekerList = db.Jobseekers.Where(u => u.UserId == id).Take(1).ToList(),
                jobseekerEducationList = db.JobSeekerEducations.Where(u => u.UserId == id).ToList(),
                

            };
            foreach (var item in model.jobseekerList)
            {
                if (item.Skills != null)
                {
                    string[] skill = item.Skills.Split(',');
                    model.skills = new List<string>();
                    for (int i = 0; i < skill.Length; i++)
                    {
                        model.skills.Add(skill[i]);

                    }
                }

            }

            return View(model);

        }


        public ActionResult AppliedJobs(string id)
        {
            var jobseekerid = db.Jobseekers.Where(u => u.UserId == id).Select(u => u.Id).ToList();
            var jobid = db.JobseekerJobs.Where(u => jobseekerid.Contains(u.JobSeekerId)).Select(u => u.JobId).ToList();

            var model = new JobseekerDashboardViewModel
            {
                jobList = (from emp in db.Employers
                           join job in db.Jobs
                            on emp.UserId equals job.UserId
                            join ca in db.jobcategories on job.CategoryId equals ca.Id
                           where jobid.Contains(job.Id)
                           select new
                           {
                               emp = emp,
                               job = job,
                              Name=ca.CategoryName

                           }).AsEnumerable().Select(c => c.ToExpando())
        };

            return View(model);
        }



        





    }


}