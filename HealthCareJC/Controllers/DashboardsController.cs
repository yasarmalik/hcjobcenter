﻿using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using HealthCareJC.Models;
using HealthCareJC.ViewModels;

namespace HealthCareJC.Controllers
{

    public class DashboardsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
       
        public DashboardsController()
        {

        }

        public DashboardsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationDbContext dbContext)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
       

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //*********** without security code verification *********************/

        public ActionResult SelectDashboard()
        {

            if (User.Identity.IsAuthenticated)
            {
                //  string[] userRoles = System.Web.Security.Roles.GetRolesForUser(User.Identity.Name);

                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                //   var roles = UserManager.GetRoles(usrid);
                if (usr.UserType == "Administrator")
                {
                    return RedirectToAction("AdminDashboard");
                }
                if (usr.UserType == "Employer")
                {
                    return RedirectToAction("EmployerDashboard");
                }
                else
                {
                    return RedirectToAction("JobSeekerDashboard");

                }

            }

            //if (roles.Contains("Administrator"))
            //{
            //    System.Web.HttpContext.Current.Session["userRole"] = "Administrator";
            //    return RedirectToAction("AdminDashBoard");

            //}
            //if (roles.Contains("Employer"))
            //{
            //    System.Web.HttpContext.Current.Session["userRole"] = "Employer";
            //    return RedirectToAction("EmployerDashboard");

            //}
            //else
            //{
            //    System.Web.HttpContext.Current.Session["userRole"] = "JobSeeker";
            //    return RedirectToAction("JobSeekerDashboard");
            //}

            //}
            else
                return RedirectToAction("ClearLogin", "Account");

        }

        //*************** If Security code verification is required /////////////////

        //public ActionResult MEDS()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        // LoginCodeViewModel vcvm= new LoginCodeViewModel();
        //        // bool codeStat = vcvm.GetCodeStatus();
        //        bool codeStatus = Convert.ToBoolean(TempData["codeStatus"]);
        //        if (codeStatus == false)
        //        {
        //            return RedirectToAction("LoginClear", "Account");
        //        }
        //        var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
        //        // if (UserManager.IsInRole(usr, "Administrator"))
        //        var roles = UserManager.GetRoles(usrid);
        //        if (roles.Contains("Administrator"))
        //        {
        //            return RedirectToAction("AdminDashBoard");
        //        }
        //        else
        //        { return RedirectToAction("WorkerDashBoard"); }

        //    }
        //    else
        //        return RedirectToAction("LoginClear", "Account");
        //}

        public ActionResult AdminDashBoard()
        {
            if (User.Identity.IsAuthenticated)
            {
                //  string[] userRoles = System.Web.Security.Roles.GetRolesForUser(User.Identity.Name);

                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);
                var model = new AdminDashboardViewModel {
                user = db.Users.Where(u => u.Id == usr.Id).ToList()
            };
            return View(model);
        }
            return RedirectToAction("ClearLogin", "Account");

        }


        public ActionResult EmployerDashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                //  string[] userRoles = System.Web.Security.Roles.GetRolesForUser(User.Identity.Name);

                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                var model = new EmployeerDashboardViewModel
                {
                    
                    user=db.Users.Where(u=>u.Id==usr.Id).ToList()
                    
                };
                return View(model);
            }
            return RedirectToAction("ClearLogin", "Account");

        }

        public ActionResult JobSeekerDashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                var model = new JobseekerDashboardViewModel
                {
                    jobseekerList = db.Jobseekers.Where(u => u.UserId == usr.Id).Take(1).ToList(),
                   


                };
                foreach (var item in model.jobseekerList)
                {
                    if (item.Skills != null)
                    {
                        string[] skill = item.Skills.Split(',');
                        model.skills = new System.Collections.Generic.List<string>();
                        for (int i = 0; i < skill.Length; i++)
                        {
                            model.skills.Add(skill[i]);

                        }
                    }

                }

                return View(model);
            }
            return RedirectToAction("ClearLogin", "Account");
        }

     
    }
}