﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using HealthCareJC.Models;
using HealthCareJC.ViewModels;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace HealthCareJC.Controllers
{
    //[Authorize]
    public class ManageCouponsController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();
        public ManageCouponsController()
        {
        }

        public ManageCouponsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult AllCoupons()
        {

            return View(db.Coupons.Where(u => u.Deleted == 0).ToList());
               
        }
        public ActionResult AllEventCoupons()
        {

            return View(db.EventCoupons.Where(u => u.Deleted == 0).ToList());

        }

        public ActionResult AddCoupon()
        {

            return View();

        }

        public ActionResult AddEventCoupon()
        {

            return View();

        }

        public ActionResult UpdateJobRate(jobrates vm)
        {

            jobrates jobrate = db.Jobrates.Where(u => u.Id >=1).SingleOrDefault();
            jobrate.Amount = vm.Amount;
            db.Entry(jobrate).State = EntityState.Modified;
            db.SaveChanges();
            ViewBag.data = "Successfully";

            return RedirectToAction("ManageJobRates", "ManageCoupons");

        }
        public ActionResult UpdateEventRate(eventrates vm)
        {

            eventrates Eventrates = db.EventRates.Where(u => u.Id >= 1).SingleOrDefault();
            Eventrates.Amount = vm.Amount;
            db.Entry(Eventrates).State = EntityState.Modified;
            db.SaveChanges();
            ViewBag.data = "Successfully";

            return RedirectToAction("ManageEventRates", "ManageCoupons");

        }
        public ActionResult ManageJobRates()
        {
            jobrates jobrate = db.Jobrates.Where(u => u.Id >= 1).SingleOrDefault();

            return View(jobrate);


        }
        public ActionResult giveJobCouponValue(string CouponName)
        {
            var coupon = db.Coupons.Where(x => x.CouponName == CouponName && x.Status == 1 && x.Deleted == 0).FirstOrDefault();
            if (coupon != null && coupon.ExpiryDate > DateTime.Now.Date)
            {

                return Json(new { amount = coupon.Amount, status = "Success" });
            }
            else
            {
                return Json(new { amount = 0,status = "Failed" });
            }
           
        }

        public ActionResult giveEventCouponValue(string CouponName)
        {
            var coupon = db.EventCoupons.Where(x => x.CouponName == CouponName && x.Status == 1 && x.Deleted == 0).FirstOrDefault();
            if (coupon != null && coupon.ExpiryDate > DateTime.Now.Date)
            {

                return Json(new { amount = coupon.Amount, status = "Success" });
            }
            else
            {
                return Json(new { amount = 0, status = "Failed" });
            }

        }

        public ActionResult ManageEventRates()
        {
            eventrates jobrate = db.EventRates.Where(u => u.Id >= 1).SingleOrDefault();

            return View(jobrate);


        }
        public ActionResult DeleteCoupon(int id)
        {

            Coupon coupon = db.Coupons.Where(u => u.Id == id).SingleOrDefault();
            coupon.Deleted = 1;
            db.Entry(coupon).State = EntityState.Modified;
            db.SaveChanges();
            ViewBag.data = "Successfully";

            return RedirectToAction("AllCoupons", "ManageCoupons");

        }
        public ActionResult DeleteEventCoupon(int id)
        {

            EventCoupon coupon = db.EventCoupons.Where(u => u.Id == id).SingleOrDefault();
            coupon.Deleted = 1;
            db.Entry(coupon).State = EntityState.Modified;
            db.SaveChanges();
            ViewBag.data = "Successfully";

            return RedirectToAction("AllEventCoupons", "ManageCoupons");

        }
        public ActionResult AddNewCoupon(Coupon vm)
        {
            Coupon coupon = new Coupon();
            coupon.CouponName = vm.CouponName;
            coupon.Amount = vm.Amount;
            coupon.ExpiryDate = vm.ExpiryDate;
            coupon.Status = 1;
            db.Coupons.Add(coupon);
            db.SaveChanges();
            ViewBag.data = "Successfully";
            return RedirectToAction("AllCoupons", "ManageCoupons");

        }
        public ActionResult AddNewEventCoupon(Coupon vm)
        {
            EventCoupon coupon = new EventCoupon();
            coupon.CouponName = vm.CouponName;
            coupon.Amount = vm.Amount;
            coupon.ExpiryDate = vm.ExpiryDate;
            coupon.Status = 1;
            db.EventCoupons.Add(coupon);
            db.SaveChanges();
            ViewBag.data = "Successfully";
            return RedirectToAction("AllEventCoupons", "ManageCoupons");

        }
        [AllowAnonymous]
        [HttpGet] 
        public JsonResult CheckCoupon(string CouponName)
        {


            try
            {
                var obj = db.Coupons.Where(u => u.CouponName == CouponName).SingleOrDefault();
                if (obj==null)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if(obj.Deleted==1)
                {
                    
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if (obj.Status == 0)
                {
                   
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if (obj.ExpiryDate < DateTime.Now.Date)
                {
                    
                    return Json(false, JsonRequestBehavior.AllowGet);
                    
                }
                else
                {
                  return Json(true, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult CheckEventCoupon(string CouponName)
        {


            try
            {
                var obj = db.EventCoupons.Where(u => u.CouponName == CouponName).SingleOrDefault();
                if (obj == null)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if (obj.Deleted == 1)
                {

                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if (obj.Status == 0)
                {

                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else if (obj.ExpiryDate < DateTime.Now.Date)
                {

                    return Json(false, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }
        
        
    }
}