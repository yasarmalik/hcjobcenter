﻿using HealthCareJC.Services;
using HealthCareJC.Models;
using HealthCareJC.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PayPal.Api;

namespace HealthCareJC.Controllers
{
    public class JobsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private JobServices jobServices = new JobServices();
        private JCEmailService jcemail = new JCEmailService();
        private ApplicationUserManager _userManager;
        // GET: Jobs
        public ActionResult PostJob()
        {
            if (User.Identity.IsAuthenticated)
            {
                 var currentDate = DateTime.Now.Date;
                var model = new JobViewModel
                {
                   
                    JobCatagory = new SelectList(jobServices.GetCatagories(), "Id", "CategoryName"),
                    JobType = jobServices.JobTypeData(),
                    eventAdList = db.EventAds.Where(x=>x.EventDate>= currentDate).OrderBy(u => u.EventDate).Take(8).ToList(),
                    CompensationType = jobServices.CompensationTypeData(),
                    ExperienceLevel = jobServices.ExperienceLevelData(),
                    JobStatus = jobServices.JobStatusData(),
                    EducationLevel = jobServices.EducationLevel(),
                    Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name"),
                    HealthProfessionalType =jobServices.HealthProfessionalType(),
                    JobRate = db.Jobrates.Select(x=>x.Amount.ToString()).Single()
                };
                return View(model);
            }
            else
            {var currentDate = DateTime.Now.Date;
                var model = new JobViewModel
                {
                     
                    JobCatagory = new SelectList(jobServices.GetCatagories(), "Id", "CategoryName"),
                    JobType = jobServices.JobTypeData(),
                    eventAdList=db.EventAds.Where(x => x.EventDate >= currentDate).OrderBy(u=>u.EventDate).Take(8).ToList(),
                    CompensationType = jobServices.CompensationTypeData(),
                    ExperienceLevel = jobServices.ExperienceLevelData(),
                    JobStatus = jobServices.JobStatusData(),
                    EducationLevel = jobServices.EducationLevel(),
                    Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name"),
                    HealthProfessionalType = jobServices.HealthProfessionalType(),
                    JobRate = db.Jobrates.Select(x => x.Amount.ToString()).Single()
                };
                return View(model);
            }
        }
        public ActionResult EditPostJob(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {

                var model = new JobViewModel
                {

                    JobCatagory = new SelectList(jobServices.GetCatagories(), "Id", "CategoryName"),
                    JobType = jobServices.JobTypeData(),
                    CompensationType = jobServices.CompensationTypeData(),
                    ExperienceLevel = jobServices.ExperienceLevelData(),
                    JobStatus = jobServices.JobStatusData(),
                    EducationLevel = jobServices.EducationLevel(),
                    jobs = db.Jobs.Where(u => u.Id == id).SingleOrDefault(),
                    Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name"),
                    HealthProfessionalType = new SelectList(jobServices.HealthProfessionalType(), "Id", "Name"),
                };
                return View(model);
            }
            else
            {
                return RedirectToAction("ClearLogin", "Account");
            }
        }


        public ActionResult JobsByCategory(int catId)
        {
            var model = new HomeSearchViewModel
            {
                jobs = (from emp in db.Employers
                        join job in db.Jobs
                         on emp.UserId equals job.UserId
                        join ca in db.jobcategories on job.CategoryId equals ca.Id
                        join sp in db.specialities on job.SpcialityId equals sp.Id
                        where job.CategoryId == catId
                        select new
                        {
                            emp = emp,
                            job = job,
                            caname = ca.CategoryName,
                            specilty = sp.Name

                        }).AsEnumerable().Select(c => c.ToExpando())


            };

            return View(model);

        }

        public ActionResult Companies()
        {
            var companies = db.Employers.ToList();

            return View(companies);
        }
        public ActionResult JobsByCompany(string companyName)
        {

            var model = new HomeSearchViewModel
            {
                jobs = (from emp in db.Employers
                        join job in db.Jobs
                         on emp.UserId equals job.UserId
                        join ca in db.jobcategories on job.CategoryId equals ca.Id
                        join sp in db.specialities on job.SpcialityId equals sp.Id
                        where emp.CompanyName.Contains(companyName)
                        select new
                        {
                            emp = emp,
                            job = job,
                            caname = ca.CategoryName,
                            specilty = sp.Name

                        }).AsEnumerable().Select(c => c.ToExpando())


            };

            return View(model);

        }

        public ActionResult AllJobs()
        {
            var jobs = db.Jobs.ToList();

            return View(jobs);
        }
        public string GetBaseUrl()
        {

            return Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port;
        }
       
        #region Single PayPal Payment
        public ActionResult CreatePayment(float amount)
        {
            var payment = PayPalPaymentService.CreatePayment(GetBaseUrl(), "sale",amount);

            return Redirect(payment.GetApprovalUrl());
        }

        public ActionResult PaymentCancelled()
        {
           // TODO: Handle cancelled payment
            return RedirectToAction("Error");
        }

        public ActionResult PaymentSuccessful(string paymentId, string token, string PayerID)
        {
            if (Session["Type"] == "JOB")
            {
                int jobid = Convert.ToInt32(Session["JobId"]);
                Job job = db.Jobs.Where(u => u.Id == jobid).SingleOrDefault();
                job.isPaid = 1;
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                // Execute Payment
                var payment = PayPalPaymentService.ExecutePayment(paymentId, PayerID);
                return RedirectToAction("ThanksPage", "Jobs");
            }else
            {
                int eventid = Convert.ToInt32(Session["EventId"]);
                EventAd even_t = db.EventAds.Where(u => u.Id == eventid).SingleOrDefault();
                even_t.isPaid = 1;
                db.Entry(even_t).State = EntityState.Modified;
                db.SaveChanges();
                // Execute Payment
                var payment = PayPalPaymentService.ExecutePayment(paymentId, PayerID);
                return RedirectToAction("ThanksPage", "Jobs");
            }
            //return View();
        }
        #endregion

        #region Authorize PayPal Payment
        public ActionResult AuthorizePayment()
        {
            var payment = PayPalPaymentService.CreatePayment(GetBaseUrl(), "authorize");

            return Redirect(payment.GetApprovalUrl());
        }

        public ActionResult AuthorizeSuccessful(string paymentId, string token, string PayerID)
        {
            // Capture Payment
            var capture = PayPalPaymentService.CapturePayment(paymentId);

            return View();
        }
        #endregion


        public ActionResult AddJobPost(JobViewModel vm)
        {
            
            Decimal? Amount = 0;
            int jobid = 0;

            Session["Type"] = "JOB";
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                if (ModelState.IsValid)
                {

                    Job job = new Job();
                    ApplicationUser user = new ApplicationUser();
                    Employer employer = new Employer();
                    try
                    {
                        
                        var catId = vm.jobs.CategoryId;
                      
                        var catName = db.jobcategories.Where(x => x.Id == catId).Select(x => x.CategoryName).FirstOrDefault().ToString();
                        job.JobNumber =  DateTime.Now.ToString("MM") + DateTime.Now.Year.ToString().Substring(2, 2) + catName.Substring(0, 3).ToUpper()+ db.Jobs.Max(x => x.Id);

                        job.CategoryId = vm.jobs.CategoryId;
                        job.SpcialityId = vm.jobs.SpcialityId;
                        job.Compensation = vm.jobs.Compensation;
                        job.CompensationType = vm.jobs.CompensationType;
                        job.CreatedDate = DateTime.Now;
                        job.EducationLevel = vm.jobs.EducationLevel;
                        job.JobTitle = vm.jobs.JobTitle;
                        job.JobType = vm.jobs.JobType;
                        job.JobStatus = vm.jobs.JobStatus;
                        job.HealthProfessionalType = vm.jobs.HealthProfessionalType;
                        job.UserId = usr.Id;
                        job.HealthProfessionalType = vm.jobs.HealthProfessionalType;

                        job.JobLocation = vm.jobs.JobLocation;
                        job.JobDescription = vm.jobs.JobDescription;
                        job.ExperienceLevel = vm.jobs.ExperienceLevel;
                        var jobrate = db.Jobrates.ToList();
                        float rate=0;
                        if(jobrate!=null)
                        rate= jobrate[0].Amount;
                        
                        var coupon = db.Coupons.Where(x => x.CouponName == vm.CouponName && x.Status == 1&&x.Deleted==0).FirstOrDefault();
                        if (coupon != null)
                        {
                            job.JobAmount = Convert.ToDecimal((rate - coupon.Amount));
                        }
                        else
                            job.JobAmount = Convert.ToDecimal(rate);
                        Amount = job.JobAmount;
                        //string fileName = "";
                        //string path = "";
                        //if (Request.Files.Count > 0)
                        //{
                        //    var file = Request.Files[0];

                        //    if (file != null && file.ContentLength > 0)
                        //    {
                        //        fileName = Path.GetFileName(file.FileName);
                        //        path = Path.Combine(Server.MapPath("~/Content/Images/Logo"), fileName);
                        //        file.SaveAs(path);
                        //        employer.Logo = fileName;

                        //    }
                        //}
                        //else
                        //{

                        //    employer.Logo = "shortlogo.png";

                        //}
                        //employer.CompanyName = vm.employer.CompanyName;
                        //employer.CompanyEmail = vm.CompanyEmail;
                        //employer.CompanyAddress = vm.employer.CompanyAddress;
                        //employer.WebSite = vm.employer.WebSite;
                        //employer.UserId = usr.Id;
                        //employer.CompanyContactNo = vm.employer.CompanyContactNo;
                        //db.Employers.Add(employer);


                        db.Jobs.Add(job);
                        db.SaveChanges();
                        ViewBag.data = "Successfully";

                        jcemail.JobPostedEmail(usr.Email, usr.FirstName + " " + usr.LastName, Url.Action("ClearLogin", "Account"));
                        jcemail.SendEmailToAdmin(usr.FirstName + " " + usr.LastName, usr.Email);


                        //todo: add code to send email to employer and admin
                        Session["JobId"] = job.Id;

                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine(
                                "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                }

                
               return RedirectToAction("CreatePayment", "Jobs", new { amount = Amount });
               
            }

            else // if user is not loged in
            {

                if (ModelState.IsValid)
                {

                    Job job = new Job();
                    RegisterViewModel rvm = new RegisterViewModel();
                    ApplicationUser user = new ApplicationUser();
                    Employer employer = new Employer();
                    Guid password = Guid.NewGuid();
                    string userID = "";
                    try
                    {
                        
                            
                            user.FirstName = vm.user.FirstName;
                            user.LastName = vm.user.LastName;
                            user.Email = vm.UserEmail;
                            user.UserName = vm.UserEmail;
                            user.PhoneNumber = vm.UserPhone;
                            string Password = vm.user.FirstName + "A@678";
                            user.UserType = "Employer";
                         //   Guid id = Guid.NewGuid();

                            
                            var result = UserManager.CreateAsync(user, Password);

                            userID = user.Id;
                        var maxJobNumber = 0;
                        var catId = vm.jobs.CategoryId;
                        var catName = db.jobcategories.Where(x => x.Id == catId).Select(x => x.CategoryName).FirstOrDefault().ToString();
                        var jobList=db.Jobs.ToList();
                         
                        if (jobList.Count == 0)
                        {
                            maxJobNumber = 0;
                        }
                        else
                        {
                            maxJobNumber = jobList.Max(x => x.Id);
                        }
                        job.JobNumber = DateTime.Now.ToString("MM") + DateTime.Now.Year.ToString().Substring(2, 2) + catName.Substring(0, 3).ToUpper() + maxJobNumber;

                        job.CategoryId = vm.jobs.CategoryId;
                        job.SpcialityId = vm.jobs.SpcialityId;
                        job.Compensation = vm.jobs.Compensation;
                        job.CompensationType = vm.jobs.CompensationType;
                        job.CreatedDate = DateTime.Now;
                        job.EducationLevel = vm.jobs.EducationLevel;
                        job.JobTitle = vm.jobs.JobTitle;
                        job.JobType = vm.jobs.JobType;
                        job.HealthProfessionalType = vm.jobs.HealthProfessionalType;

                        job.UserId = userID;

                        job.JobLocation = vm.jobs.JobLocation;
                        job.JobDescription = vm.jobs.JobDescription;
                        job.ExperienceLevel = vm.jobs.ExperienceLevel;
                        var jobrate = db.Jobrates.ToList();
                        float rate = 0;
                        if (jobrate != null)
                            rate = jobrate[0].Amount;
                        var coupon = db.Coupons.Where(x => x.CouponName == vm.CouponName && x.Status == 1 && x.Deleted == 0).FirstOrDefault();
                        if (coupon != null)
                        {
                            job.JobAmount = Convert.ToDecimal((rate - coupon.Amount));
                        }
                        else
                            job.JobAmount =  Convert.ToDecimal(rate);
                        Amount = job.JobAmount;
                        db.Jobs.Add(job);
                        string fileName = "";
                        string path = "";
                       
                        if (Request.Files.Count > 0)
                        {
                            var file = Request.Files[0];

                            if (file != null && file.ContentLength > 0)
                            {
                                fileName = Path.GetFileName(file.FileName);
                                path = Path.Combine(Server.MapPath("~/Content/Images/Logo"), fileName);
                                file.SaveAs(path);
                                employer.Logo = fileName;

                            }
                        }
                        else
                        {

                            employer.Logo = "shortlogo.png";

                        }
                        employer.CompanyName = vm.employer.CompanyName;
                        employer.CompanyEmail = vm.CompanyEmail;
                        employer.CompanyAddress = vm.employer.CompanyAddress;
                        employer.WebSite = vm.employer.WebSite;
                        employer.UserId = userID;
                        employer.CompanyContactNo = vm.employer.CompanyContactNo;
                        db.Employers.Add(employer);
                        db.SaveChanges();
                        Session["JobId"] = job.Id;

                         jcemail.EmailPasswordToNewUser(vm.UserEmail, vm.user.FirstName + " " + vm.user.LastName, Url.Action("ClearLogin", "Account"), Password);
                       jcemail.SendEmailToAdmin(vm.user.FirstName + " " + vm.user.LastName,vm.UserEmail);


                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine(
                                "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                }
                return RedirectToAction("CreatePayment", "Jobs", new { amount = Amount });
            }



        }


        public ActionResult SaveEditJobPost(JobViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                if (ModelState.IsValid)
                {

                    Job job = db.Jobs.Where(u => u.Id == vm.jobs.Id).SingleOrDefault();


                    try
                    {
                        job.JobNumber = vm.jobs.JobNumber;
                        job.CategoryId = vm.jobs.CategoryId;
                        job.SpcialityId = vm.jobs.SpcialityId;
                        job.Compensation = vm.jobs.Compensation;
                        job.CompensationType = vm.jobs.CompensationType;
                        job.CreatedDate = DateTime.Now;
                        job.EducationLevel = vm.jobs.EducationLevel;
                        job.JobTitle = vm.jobs.JobTitle;
                        job.JobType = vm.jobs.JobType;
                        job.JobStatus = vm.jobs.JobStatus;
                        job.HealthProfessionalType = vm.jobs.HealthProfessionalType;
                        job.UserId = vm.jobs.UserId;
                        job.JobLocation = vm.jobs.JobLocation;
                        job.ExperienceLevel = vm.jobs.ExperienceLevel;
                        db.Jobs.Attach(job);
                        db.Entry(job).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.data = "Successfully";

                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine(
                                "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                }
                return RedirectToAction("EmployerDashboard", "Dashboards");
            }


            return RedirectToAction("ClearLogin", "Account");


        }

        public ActionResult JobDetail(int id)
        {
            int isAdmin = 0;
            
            if (User.Identity.IsAuthenticated)
            {
                
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();
               
                var usr = db.Users.Find(usrid);
                
                //var usr_role=db.
                isAdmin=(usr.UserType=="Administrator")?1:0;
                var getjobseekerId = db.Jobseekers.Where(u => u.UserId == usr.Id).Select(u => u.Id).ToList();
                var appliedjobs = db.JobseekerJobs.Where(u => getjobseekerId.Contains(u.JobSeekerId)).Select(u => u.JobId).ToList();
                if (appliedjobs.Contains(id))
                {
                    ViewBag.job = "true";
                }


            }

            int jobid = Convert.ToInt32(id);
           
            if (isAdmin == 1)
            {
                JobDetailViewModel model = new JobDetailViewModel
                {
                    joblist = (from emp in db.Employers
                               join job in db.Jobs on emp.UserId equals job.UserId
                               join ca in db.jobcategories on job.CategoryId equals ca.Id
                               join sp in db.specialities on job.SpcialityId equals sp.Id
                               join us in db.Users on job.UserId equals us.Id
                               where job.Id == jobid
                               select new
                               {
                                   emp = emp,
                                   job = job,
                                   caname = ca.CategoryName,
                                   specilty = sp.Name,
                                   user=us
                               }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando())
                               

                };
               
                ViewBag.user = User;
                return View(model);
               // return View("JobDetailsAdmin",model);
            }
            else
            {
                var model = new JobDetailViewModel
                {
                    joblist = (from emp in db.Employers
                               join job in db.Jobs
                                on emp.UserId equals job.UserId
                               join ca in db.jobcategories on job.CategoryId equals ca.Id
                               join sp in db.specialities on job.SpcialityId
                               equals sp.Id
                               where job.Id == jobid
                               select new
                               {
                                   emp = emp,
                                   job = job,
                                   caname = ca.CategoryName,
                                   specilty = sp.Name
                               }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando())


                };
                return View(model);
            }
        }


        public ActionResult JobDetailsAdmin(int id)
        {
            int isAdmin = 0;

            if (User.Identity.IsAuthenticated)
            {

                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                //var usr_role=db.
                isAdmin = (usr.UserType == "Administrator") ? 1 : 0;
                var getjobseekerId = db.Jobseekers.Where(u => u.UserId == usr.Id).Select(u => u.Id).ToList();
                var appliedjobs = db.JobseekerJobs.Where(u => getjobseekerId.Contains(u.JobSeekerId)).Select(u => u.JobId).ToList();
                if (appliedjobs.Contains(id))
                {
                    ViewBag.job = "true";
                }


            }

            int jobid = Convert.ToInt32(id);

            if (isAdmin == 1)
            {
                JobDetailViewModel model = new JobDetailViewModel
                {
                    joblist = (from emp in db.Employers
                               join job in db.Jobs on emp.UserId equals job.UserId
                               join ca in db.jobcategories on job.CategoryId equals ca.Id
                               join sp in db.specialities on job.SpcialityId equals sp.Id
                               join us in db.Users on job.UserId equals us.Id
                               where job.Id == jobid
                               select new
                               {
                                   emp = emp,
                                   job = job,
                                   caname = ca.CategoryName,
                                   specilty = sp.Name,
                                 user = us
                               }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando())


                };

                ViewBag.user = User;

                var i = model.joblist;
                int k = i.Count();
               
                
               
                return View(model);
                // return View("JobDetailsAdmin",model);
            }
            else
            {
                var model = new JobDetailViewModel
                {
                    joblist = (from emp in db.Employers
                               join job in db.Jobs
                                on emp.UserId equals job.UserId
                               join ca in db.jobcategories on job.CategoryId equals ca.Id
                               join sp in db.specialities on job.SpcialityId
                               equals sp.Id
                               where job.Id == jobid
                               select new
                               {
                                   emp = emp,
                                   job = job,
                                   caname = ca.CategoryName,
                                   specilty = sp.Name
                               }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando())


                };
                return View(model);
            }
        }

        public ActionResult LoginCheck(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Session["id"] = id;

                return Content("true");
            }
            else
            {


                Session["id"] = id;

                return Content("false");
            }



        }

        public ActionResult SaveResume(HttpPostedFileBase files, string Text)
        {

            try
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);

                JobSeekerJob jobseekerjob = new JobSeekerJob();

                string fileName = "";
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        fileName = Session["id"].ToString();
                        string path = Path.Combine(Server.MapPath("~/Resume"), fileName);
                        file.SaveAs(path);
                    }
                }
                int jobid = Convert.ToInt32(Session["id"]);
                jobseekerjob.CoverLetter = Text;
                jobseekerjob.ResumePath = fileName;
                int jobseekerId = db.Jobseekers.Where(u => u.UserId == usr.Id).Select(u => u.Id).FirstOrDefault();
                jobseekerjob.JobId = Convert.ToInt32(Session["id"]);
                jobseekerjob.JobSeekerId = jobseekerId;
                jobseekerjob.ApplyDate = DateTime.Now;
                db.JobseekerJobs.Add(jobseekerjob);
                db.Entry(jobseekerjob).State = EntityState.Added;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                throw;
            }

            return View("true");





        }

        public ActionResult ManageJobs()
        {
            ViewBag.usertype="notype";
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);
                if (usr.UserType == "Administrator")
                {
                    var model = new ManageJobViewModel
                    {
                        jobList = db.Jobs.ToList(),


                    };

                    ViewBag.usertype = "Administrator";
                    return View(model);
                }
                else
                {

                    var model = new ManageJobViewModel
                    {
                        jobList = db.Jobs.Where(u => u.UserId == usr.Id).ToList(),


                    };
                    model.count = new Dictionary<int, string>();
                    foreach (var item in model.jobList)
                    {
                        var obj = db.JobseekerJobs.Where(u => u.JobId == item.Id).Count().ToString();

                        model.count.Add(item.Id, obj.ToString());

                    }


                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("ClearLogin", "Account");
            }


        }


        public ActionResult DeleteJob(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var job = new Job { Id = id };
                db.Entry(job).State = EntityState.Deleted;
                db.SaveChanges();

                return RedirectToAction("ManageJobs");
            }
            return RedirectToAction("ClearLogin", "Account");
        }

        public ActionResult ManageApplications(int? id)
        {
            ManageApplicationViewModel manageapplciation = new ManageApplicationViewModel();
            List<JobSeeker> jobseeker = new List<JobSeeker>();
            var obj = db.JobseekerJobs.Where(u => u.JobId == id).Select(u => u.JobSeekerId).ToList();
            foreach (var item in obj)
            {
                var jobseekerRecored = db.Jobseekers.Where(u => u.Id == item).SingleOrDefault();
                jobseeker.Add(jobseekerRecored);

            }
            manageapplciation.jobseekerList = jobseeker;



            return View(manageapplciation);

        }
        public ActionResult EditEmployeerProfile(string userid)
        {
           
            var model = new EditCompanyProfileViewModel
            {
                user = db.Users.Where(u => u.Id == userid).SingleOrDefault()

            };


            return View(model);

        }
        public ActionResult SaveCompanyProfile(EditCompanyProfileViewModel vm)
        {
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);



                ApplicationUser user = db.Users.Where(u => u.Id == usr.Id).SingleOrDefault();
                string path = "";

                try
                {


                    user.FirstName = vm.user.FirstName;
                    user.LastName = vm.user.LastName;
                    user.PhoneNumber = vm.user.PhoneNumber;
                    user.Email = vm.user.Email;
                    
                    db.Users.Attach(user);
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.data = "Successfully";

                    return RedirectToAction("EmployerDashboard", "Dashboards");




                }
                catch (Exception)
                {
                }

            }
            return RedirectToAction("ClearLogin", "Account");
        }

        public ActionResult PublishJob(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.Jobs.Where(u => u.Id == id).SingleOrDefault();
                obj.status = true;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageJobs", "Jobs");


            }
            return RedirectToAction("ClearLogin", "Account");
        }


        public ActionResult disApprove(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.Jobs.Where(u => u.Id == id).SingleOrDefault();
                obj.status = false;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageJobs", "Jobs");


            }
            return RedirectToAction("ClearLogin", "Account");
        }
        public JsonResult CheckExistingEmailCompany(string CompanyEmail)
        {


            try
            {
                var obj = db.Employers.Where(u => u.CompanyEmail == CompanyEmail).ToList();
                if (obj.Count > 0)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult CheckExistingEmailUser(string UserEmail)
        {


            try
            {
                var obj = db.Users.Where(u => u.Email == UserEmail).ToList();
                if (obj.Count > 0)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult ThanksPage()
        {

            return View();
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

    }
    
}