﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthCareJC.Models;
using HealthCareJC.Services;

namespace HealthCareJC.Controllers
{
    public class SubscribersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Subscribers
        public ActionResult AddSubscriber(FormCollection frmCollection)
        {
            Subscriber subscriber=new Subscriber();
            subscriber.FullName= frmCollection["fullname"].ToString();
            subscriber.Email = frmCollection["email"].ToString();
            subscriber.Phone = frmCollection["phone"].ToString();
            subscriber.Active = true;

            db.Subscribers.Add(subscriber);
            db.SaveChanges();
     
            JCEmailService emailService= new JCEmailService();
            emailService.SendEmailToSubscriber(subscriber.Email,subscriber.FullName);

            if (subscriber.Phone != null || subscriber.Phone != "")
            {
                JCSmsService smsService= new JCSmsService();
                smsService.SendSmsToSubscriber(subscriber.Phone,subscriber.FullName);
            }

            return RedirectToAction("ThanksSubscriber");
        }
       
        public ActionResult ThanksSubscriber()
        {
            return View();
        }
    }
}