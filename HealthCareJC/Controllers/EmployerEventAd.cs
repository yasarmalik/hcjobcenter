﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class EmployerEventAd
    {
        public int Id { get; set; }
        public int EmployerId { get; set; }
        public int EventAdId { get; set; }
    }
}