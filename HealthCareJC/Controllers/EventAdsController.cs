﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthCareJC.Models;
using Microsoft.AspNet.Identity;
using HealthCareJC.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using HealthCareJC.Services;

namespace HealthCareJC.Controllers
{
    public class EventAdsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        private JCEmailService jcemail = new JCEmailService();
        // GET: EventAds
       public ActionResult EventDetail(int eventId)
        {
            var model = new EventAdViewsModel
            {
                eventlist = (from emp in db.Employers
                           join events in db.EventAds
                            on emp.UserId equals events.UserId

                           where events.Id== eventId


                             select new
                           {
                               emp = emp,
                               events = events
                           }).AsEnumerable().Select(c => c.ToExpando())


            };

            return View(model);

        }
       public string GetBaseUrl()
       {

           return Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port;
       }

       #region Single PayPal Payment
       public ActionResult CreatePayment(float amount)
       {
           var payment = PayPalPaymentService.CreatePayment(GetBaseUrl(), "sale", amount);

           return Redirect(payment.GetApprovalUrl());
       }

       public ActionResult PaymentCancelled()
       {
           // TODO: Handle cancelled payment
           return RedirectToAction("Error");
       }

       public ActionResult PaymentSuccessful(string paymentId, string token, string PayerID)
       {
           int eventid = Convert.ToInt32(Session["EventId"]);
           EventAd even_t = db.EventAds.Where(u => u.Id == eventid).SingleOrDefault();
           even_t.isPaid = 1;
           db.Entry(even_t).State = EntityState.Modified;
           db.SaveChanges();
           // Execute Payment
           var payment = PayPalPaymentService.ExecutePayment(paymentId, PayerID);
           return RedirectToAction("ThanksPage", "Jobs");
           //return View();
       }
       #endregion

       #region Authorize PayPal Payment
       public ActionResult AuthorizePayment()
       {
           var payment = PayPalPaymentService.CreatePayment(GetBaseUrl(), "authorize");

           return Redirect(payment.GetApprovalUrl());
       }

       public ActionResult AuthorizeSuccessful(string paymentId, string token, string PayerID)
       {
           // Capture Payment
           var capture = PayPalPaymentService.CapturePayment(paymentId);

           return View();
       }
       #endregion

       public ActionResult ManageEvents()
        {
            var currentDate = DateTime.Now.Date;
            if (User.Identity.IsAuthenticated)
            {
                var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var usr = db.Users.Find(usrid);
                var user = db.Users.Where(u => u.Id == usr.Id).SingleOrDefault();
                if (user.UserType == "Administrator")
                {
                    var model = new ManageEventsViewModel
                    {
                        eventads= db.EventAds.OrderBy(u => u.EventDate >= currentDate).ToList()

                    };

                    return View(model);
                }
                else
                {
                   
                    var model = new ManageEventsViewModel
                    {
                        eventads = db.EventAds.Where(u => u.UserId == usr.Id).ToList()

                };
                    return View(model);
                }

            }
            return RedirectToAction("ClearLogin", "Account");

        }

        // GET: EventAds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventAd eventAd = db.EventAds.Find(id);
            if (eventAd == null)
            {
                return HttpNotFound();
            }
            return View(eventAd);
        }

        // GET: EventAds/Create
        public ActionResult Create(string UserId)
        {
            var currentDate = DateTime.Now.Date;
            ViewBag.eventCatagory = db.Eventcategories.ToList();
            var model = new EventAdViewsModel
            {
               // jobList = db.Jobs.OrderBy(u => u.CreatedDate).Take(10).ToList(),
                eventAdList = db.EventAds.OrderBy(u => u.EventDate >=currentDate).Take(6).ToList(),
                EventRate=   db.EventRates.ToList()[0].Amount,
                 
                       

            };
                return View(model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create (EventAdViewsModel vm)
        {
            Decimal? Amount = 0;
            int jobid = 0;
            Session["Type"] = "EVENT";
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    var usrid = System.Web.HttpContext.Current.User.Identity.GetUserId();

                    var usr = db.Users.Find(usrid);
                    vm.eventad.UserId = usr.Id;

                    var catId = vm.eventad.EventCategoryId;
                   
                    var catName = db.Eventcategories.Where(x => x.Id == catId).Select(x => x.Name).FirstOrDefault().ToString();
                    vm.eventad.EventNumber= DateTime.Now.ToString("MM") + DateTime.Now.Year.ToString().Substring(2, 2) + catName.Substring(0, 3).ToUpper() + db.EventAds.Max(x => x.Id);

                    var eventrate = db.EventRates.ToList();
                    float rate = 0;
                    if (eventrate != null)
                        rate = eventrate[0].Amount;

                    var coupon = db.EventCoupons.Where(x => x.CouponName == vm.CouponName && x.Status == 1 && x.Deleted == 0).FirstOrDefault();
                    if (coupon != null)
                    {
                        vm.eventad.EventAmount = Convert.ToDecimal((rate - coupon.Amount));
                    }
                    else
                        vm.eventad.EventAmount = Convert.ToDecimal(rate);
                    Amount = vm.eventad.EventAmount;
                    vm.eventad.EventAmount = Amount;
                    vm.eventad.status = false;
                    db.EventAds.Add(vm.eventad);
                    db.SaveChanges();
                    Session["EventId"] = vm.eventad.Id;
                    jcemail.EventPostedEmail(usr.Email, usr.FirstName + " " + usr.LastName, Url.Action("ClearLogin", "Account"));
                    jcemail.SendEmailToAdminEvent(usr.FirstName + " " + usr.LastName, usr.Email);
   
                }
               
                return RedirectToAction("CreatePayment", "Jobs", new { amount = Amount });
            }
            else {
                string userID = "";
                if (ModelState.IsValid)
                {
                    
                        ApplicationUser user = new ApplicationUser();

                        user.FirstName = vm.user.FirstName;
                        user.LastName = vm.user.LastName;
                        user.Email = vm.UserEmail;
                        user.UserName = vm.UserEmail;
                        user.PhoneNumber = vm.user.PhoneNumber;
                        string Password = vm.user.FirstName + "A@678";
                        user.UserType = "Employer";
                        var result = UserManager.CreateAsync(user, Password);

                        userID = user.Id;
                    
                    vm.eventad.UserId = userID;
                    vm.eventad.status = false;
                    var catId = vm.eventad.EventCategoryId;
                    var catName = db.Eventcategories.Where(x => x.Id == catId).Select(x => x.Name).FirstOrDefault().ToString();
                    vm.eventad.EventNumber = DateTime.Now.ToString("MM") + DateTime.Now.Year.ToString().Substring(2, 2) + catName.Substring(0, 3).ToUpper() + db.EventAds.Max(x => x.Id);
                    var eventrate = db.EventRates.ToList();
                    float rate = 0;
                    if (eventrate != null)
                        rate = eventrate[0].Amount;

                    var coupon = db.EventCoupons.Where(x => x.CouponName == vm.CouponName && x.Status == 1 && x.Deleted == 0).FirstOrDefault();
                    if (coupon != null)
                    {
                        vm.eventad.EventAmount = Convert.ToDecimal((rate - coupon.Amount));
                    }
                    else
                        vm.eventad.EventAmount = Convert.ToDecimal(rate);
                    Amount = vm.eventad.EventAmount;
                    vm.eventad.EventAmount = Amount;
                    db.EventAds.Add(vm.eventad);
                    db.SaveChanges();
                    Session["EventId"] = vm.eventad.Id;
                    jcemail.EmailPasswordToNewUserEvent(vm.UserEmail, vm.user.FirstName + " " + vm.user.LastName, Url.Action("ClearLogin", "Account"), Password);
                    jcemail.SendEmailToAdminEvent(vm.user.FirstName + " " + vm.user.LastName,vm.UserEmail);

                }
                return RedirectToAction("CreatePayment", "Jobs", new { amount = Amount });
            }
           
        }

        // GET: EventAds/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.eventCatagory = db.Eventcategories.ToList();
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                EventAd eventAd = db.EventAds.Find(id);
                if (eventAd == null)
                {
                    return HttpNotFound();
                }
                return View(eventAd);
            }
            return RedirectToAction("ClearLogin", "Account");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,EventDate,time,EventCategoryId,Address,City,State,Zipcode,ContactPerson,ContactPhone,ContactEmail,Website,UserId,Status,EventNumber")] EventAd eventAd)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(eventAd).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.eventCatagory = db.Eventcategories.ToList();
                }
                return RedirectToAction("EmployerDashboard","Dashboards");
            }
            return RedirectToAction("ClearLogin", "Account");
        }

        public ActionResult PublishEvent(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.EventAds.Where(u => u.Id == id).SingleOrDefault();
                obj.status = true;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageEvents", "EventAds");
                


            }
            return RedirectToAction("ClearLogin", "Account");
        }
        public ActionResult disApprove(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.EventAds.Where(u => u.Id == id).SingleOrDefault();
                obj.status = false;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageEvents", "EventAds");


            }
            return RedirectToAction("ClearLogin", "Account");
        }

        public ActionResult AllEvents()
        {
            var currentDate = DateTime.Now.Date;
            var obj = db.EventAds.Where(x => x.EventDate >= currentDate).OrderBy(u => u.EventDate).ToList();
            return View(obj);
        }

        public ActionResult DeleteConfirmed(int id)
        {
            EventAd eventAd = db.EventAds.Find(id);
            db.EventAds.Remove(eventAd);
            db.SaveChanges();
            return RedirectToAction("AdminDashboard", "Dashboards");
        }
        public JsonResult CheckExistingEmailUser(string UserEmail)
        {


            try
            {
                var obj = db.Users.Where(u => u.Email == UserEmail).ToList();
                if (obj.Count > 0)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
