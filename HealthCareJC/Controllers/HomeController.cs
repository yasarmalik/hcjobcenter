﻿using HealthCareJC.ViewModels;
using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthCareJC.Services;

namespace HealthCareJC.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        private JobServices jobServices = new JobServices();
        public ActionResult Index()
        {
            var model = new HomeSearchViewModel {
                jobs = (from emp in db.Employers
                        join job in db.Jobs
                         on emp.UserId equals job.UserId
                        join ca in db.jobcategories on job.CategoryId
                        equals ca.Id
                        select new
                        {
                            emp = emp,
                            job = job,
                            name = ca.CategoryName
                        }).OrderByDescending(u => u.job.CreatedDate).Take(10).AsEnumerable().Select(c => c.ToExpando()),
                jobCategory = db.jobcategories.ToList()
                
            }; 
                
            return View(model);
        }

        public ActionResult SelectJobCategory()
        {
            var model = new HomeSearchViewModel
            {
                jobs = db.Jobs.OrderByDescending(u => u.CreatedDate).Take(10).ToList()

            };

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult SearchResult(HomeSearchViewModel vm, FormCollection frm)
        {



            if (vm.specialityid <= 0)
            {
                if (vm.category == 4)
                {
                    string[] locations = vm.location.Split(',');

                    var model = new HomeSearchViewModel
                    {
                        jobs = (from emp in db.Employers
                                join job in db.Jobs
                                on emp.UserId equals job.UserId
                                join ca in db.jobcategories on job.CategoryId equals ca.Id
                                join sp in db.specialities on job.SpcialityId
                                equals sp.Id
                                where ( job.JobLocation.Contains(vm.location) && job.status == true)

                                select new
                                {
                                    emp = emp,
                                    job = job,
                                    caname = ca.CategoryName,
                                    specilty = sp.Name
                                }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando()),

                        Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name")

                    };

                    return View(model);
                }
                else
                {
                    string[] locations = vm.location.Split(',');

                    var model = new HomeSearchViewModel
                    {
                        jobs = (from emp in db.Employers
                                join job in db.Jobs
                                on emp.UserId equals job.UserId
                                join ca in db.jobcategories on job.CategoryId equals ca.Id
                                join sp in db.specialities on job.SpcialityId
                                equals sp.Id
                                where (vm.category == job.CategoryId && job.JobLocation.Contains(vm.location) && job.status == true)

                                select new
                                {
                                    emp = emp,
                                    job = job,
                                    caname = ca.CategoryName,
                                    specilty = sp.Name
                                }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando()),

                        Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name")

                    };

                    return View(model);
                }

            }
            else
            {

                string[] locations = vm.location.Split(',');

                var model = new HomeSearchViewModel
                {
                    jobs = (from emp in db.Employers
                            join job in db.Jobs
                            on emp.UserId equals job.UserId
                            join ca in db.jobcategories on job.CategoryId equals ca.Id
                            join sp in db.specialities on job.SpcialityId
                            equals sp.Id
                            where (vm.category == job.CategoryId && job.JobLocation.Contains(vm.location) && job.status == true && vm.specialityid == job.SpcialityId)

                            select new
                            {
                                emp = emp,
                                job = job,
                                caname = ca.CategoryName,
                                specilty = sp.Name
                            }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando()),

                    Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name")

                };

                return View(model);
            }

            
            
        }
        public ActionResult SearchJob(HomeSearchViewModel vm)
        {


            if (string.IsNullOrEmpty(vm.MdNpPa))
            {
                vm.MdNpPa = "MD,NP,PA";
            }
            else
            {
                vm.MdNpPa = "";
            }

            if (string.IsNullOrEmpty(vm.NursesMedAsst))
            {
                vm.NursesMedAsst = "Nurses,Med Asst";
            }
            else
            {
                vm.NursesMedAsst = "";
            }

            if (string.IsNullOrEmpty(vm.TechsIt))
            {
                vm.TechsIt = "Techs/IT";
            }
            else
            {
                vm.TechsIt = "";
            }
            if (string.IsNullOrEmpty(vm.AdminStaff))
            {
                vm.AdminStaff = "Admin Staff";
            }
            else
            {
                vm.AdminStaff = "";
            }





            if (vm.category == 4) // 4 is for all facilities in db
                {
             

                    var model = new HomeSearchViewModel
                    {
                        jobs = (from emp in db.Employers
                                join job in db.Jobs
                                on emp.UserId equals job.UserId
                                join ca in db.jobcategories on job.CategoryId equals ca.Id
                                join sp in db.specialities on job.SpcialityId
                                equals sp.Id
                            where (job.JobLocation.Contains(vm.location) && job.status == true)
                            
                            && (job.HealthProfessionalType.Contains(vm.AdminStaff) || job.HealthProfessionalType.Contains(vm.TechsIt) || job.HealthProfessionalType.Contains(vm.MdNpPa) ||job.HealthProfessionalType.Contains(vm.NursesMedAsst))
                                select new
                                {
                                    emp = emp,
                                    job = job,
                                    caname = ca.CategoryName,
                                    specilty = sp.Name
                                }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando()),

                        Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name")

                    };

                    return PartialView("PartialViews/_SearchResult", model);
                }
                else
                {
                    // string[] locations = vm.location.Split(',');
                    var model = new HomeSearchViewModel
                    {
                        jobs = (from emp in db.Employers
                                join job in db.Jobs
                                on emp.UserId equals job.UserId
                                join ca in db.jobcategories on job.CategoryId equals ca.Id
                                join sp in db.specialities on job.SpcialityId
                                equals sp.Id
                                where (vm.category == job.CategoryId&&job.JobLocation.Contains(vm.location) && job.status == true)

                                  && (job.HealthProfessionalType==vm.AdminStaff || job.HealthProfessionalType==vm.TechsIt || job.HealthProfessionalType==vm.MdNpPa || job.HealthProfessionalType==vm.NursesMedAsst)

                                select new
                                {
                                    emp = emp,
                                    job = job,
                                    caname = ca.CategoryName,
                                    specilty = sp.Name
                                }).OrderByDescending(u => u.job.CreatedDate).AsEnumerable().Select(c => c.ToExpando()),

                        Specility = new SelectList(jobServices.Getspecialities(), "Id", "Name")

                    };

                    return PartialView("PartialViews/_SearchResult", model);

                }
            
           



        }
    }
}