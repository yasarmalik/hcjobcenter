﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View(db.Users.Where(u=>u.UserType=="Employer").ToList());
        }


        public ActionResult disApprove(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.Users.Where(u => u.Id == id).SingleOrDefault();
                obj.Approve = false;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageJobseeker", "Users");


            }
            return RedirectToAction("ClearLogin", "Account");
        }

        public ActionResult Approve(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var obj = db.Users.Where(u => u.Id == id).SingleOrDefault();
                obj.Approve = true;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ManageJobseeker", "Users");


            }
            return RedirectToAction("ClearLogin", "Account");
        }
        public ActionResult ManageJobseeker()
        {
            return View(db.Users.Where(u => u.UserType == "JobSeeker").ToList());
        }



    }
}