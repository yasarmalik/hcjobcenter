﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class JobSeekerExperience
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string JobType { get; set; } ////PartTime, Full Time, Temporary, Freelance, Internship
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]

        public DateTime EndDate { get; set; }
        public string Responsiblities { get; set; }
        public bool Continue { get; set; }
    }
}