﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class EventCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}