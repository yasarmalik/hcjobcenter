﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class JobSeekerJob
    {
        public int Id { get; set; }
      public int JobId { get; set; }

        public int JobSeekerId { get; set; } 

        public string CoverLetter { get; set; }
        public string ResumePath { get; set; }
        public DateTime ApplyDate { get; set; }
    }
}