﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class EmployerJob
    {
        public int Id { get; set; }
        public int JobId { get; set; }

        public int EmployerId { get; set; }

    }
}