﻿using System.ComponentModel.DataAnnotations;
namespace HealthCareJC.Models
{
   
    public class Permission
  {
        public int Id { get; set; } 
        [Required]
        public string Name { get; set; }
        [Display(Name="Section")]
        [Required]
        public string Controller { get; set; }
        [Required]
        [Display(Name = "Operation")]
        public string Action { get; set; }
        public bool Deleted { get; set; }
       
  }

  public class Role
  {
    public int RoleId { get; set; }
        [Required]
        public string Name { get; set; }
  }

  public class RolePermission
  {
    public int Id { get; set; }
        [Required]
        public string Role { get; set; }

    public int PermissionId { get; set; }
  }

  public class MedsUser
  {
    public int Id { get; set; }

    public string UserName { get; set; }
  }
}