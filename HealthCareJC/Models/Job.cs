﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Models
{
    public class Job
    {
        public int Id { get; set; }
        [Required]
        public string JobLocation { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public int SpcialityId { get; set; }
        

    [Required]
        public string JobTitle { get; set; }

        [AllowHtml]
        [Required]
        public string JobDescription { get; set; }
        public string Compensation { get; set; }
        public string CompensationType { get; set; } //hour, month,year
        public string JobType { get; set; } //PartTime, Full Time, Temporary, Freelance, Internship
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]

        public string JobNumber { get; set; }
        public int isPaid { get; set; }
        public string EducationLevel { get; set; }
        public string ExperienceLevel { get; set; }//Enter level //Middle level/ senior level
        public string JobStatus { get; set; }//Live, Canceled, Expired
        public string UserId { get; set; }
        public decimal? JobAmount { get; set; }


        public string HealthProfessionalType { get; set; }
         public bool status { get; set; }
        public bool approved { get; set; }
        [ForeignKey("CategoryId")]
        public virtual JobCategory jobCatrgory { get; set; }

       
        [ForeignKey("SpcialityId")]
        public virtual Speciality speciality { get; set; }

    }
}