﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HealthCareJC.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser

    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Approve { get; set; }
        public string UserType { get; set; } // JobSeeker, Employer
       
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("HealthCareJC", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public System.Data.Entity.DbSet<HealthCareJC.Models.JobCategory> jobcategories { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.Job> Jobs { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.JobSeeker> Jobseekers { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.JobSeekerJob> JobseekerJobs { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.JobSeekerEducation> JobSeekerEducations { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.JobSeekerExperience> JobSeekerExperiences { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.Subscriber> Subscribers { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.Employer> Employers { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.Coupon> Coupons { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.EventAd> EventAds { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.EventCategory> Eventcategories { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.Speciality> specialities { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.jobrates> Jobrates { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.EventCoupon> EventCoupons { get; set; }
        public System.Data.Entity.DbSet<HealthCareJC.Models.eventrates> EventRates { get; set; }
        
       
    }
}