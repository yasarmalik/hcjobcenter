﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Models
{
    public class Employer
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Logo { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string CompanyAddress { get; set; }
       // [DataType(DataType.Url)]
        public string WebSite { get; set; }
        
        public string CompanyEmail { get; set; }
        [Required]
        public string CompanyContactNo { get; set; }

    }
}