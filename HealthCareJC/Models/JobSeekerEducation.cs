﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class JobSeekerEducation
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string Degree { get; set; }
        public string School { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }
        public string Percentage { get; set; }
        public bool Continue { get; set; }
       
    }
}