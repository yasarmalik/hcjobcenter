﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class JobCoupons
    {
        int Id { get; set; }
        string  CouponName { get; set; }
        string Amount { get; set; }
        bool Status { get; set; }
    }
}