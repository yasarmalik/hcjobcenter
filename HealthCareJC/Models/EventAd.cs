﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace HealthCareJC.Models
{
    public class EventAd
    {
        public int Id { get; set; }
        public string EventNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EventDate { get; set; }
        public string time { get; set; }
        public int EventCategoryId { get; set; }  //Family Event, Wedding, Marriage, BirthDay
        public string Address { get; set; } //google API
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string UserId { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public bool status { get; set; }
        public string Website { get; set; }
        public decimal? EventAmount { get; set; }
        //public Decimal EventAmout { get; set; }
        [ForeignKey("EventCategoryId")]
        public virtual EventCategory eventCatrgory { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public int isPaid { get; set; }
             
       
       
    }
}