﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Models
{
    public class JobSeeker
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        public string CurrentAddress { get;set; }
       
        public string Skills { get; set; }
        
       

      
        public string LinkedInResume { get; set; }
        public string ProfessionalTitle { get; set; }


        public string PicturePath { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser user { get; set; }


    }
}