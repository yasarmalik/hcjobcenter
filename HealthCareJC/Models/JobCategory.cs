﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareJC.Models
{
    public class JobCategory
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public string Icon { get; set; }
    }
}