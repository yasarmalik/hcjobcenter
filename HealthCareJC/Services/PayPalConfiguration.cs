﻿using PayPal.Api;
using System.Collections.Generic;

namespace HealthCareJC.Services
{
    public static class PayPalConfiguration
    {
        public readonly static string ClientId;
        public readonly static string ClientSecret;

        // Static constructor for setting the readonly static members.
        static PayPalConfiguration()
        {
            var config = GetConfig();
            ClientId = config["clientId"];
            ClientSecret = config["clientSecret"];
        }

        // Create the configuration map that contains mode and other optional configuration details.
        public static Dictionary<string, string> GetConfig()
        {
            // ConfigManager.Instance.GetProperties(); // it doesn't work on ASPNET 5
            return new Dictionary<string, string>() {
               // { "clientId", "AXmj18LQclo2B6M7gmvtSnMDViRkr8QWk3hlzZoYsmCr3LMaa1-GO9p8tg2dsS8j4KjgJ_fyjJ0wvfXK" },
             //{ "clientSecret", "ECO8iCVdlE2toah_lsYqXQHnn9G9zlcDBC6ZMhCOqfbgluNYcxmhQAop2GcrQMM3d92aXUoyUPFgJOfu" }
               //sandbox { "clientId", "Ae1WMTcYhRWqIAPPKOrXNyr-30-4eyDhzgh2AoGYO1RNXCa9ZaApkmpgf-uOtNiH9EIHCOZfwG5JdzkJ" },
                //sandbox { "clientSecret", "EJ4ylL2idETbItiLbGw7xiJAK2YYqN4xnDbcOHBGIN4B_Y6wm3tbn5z8eb9eeLzLPCLVAEE2iaWy9MO-" }
       /*Live*/       { "clientId", "AZ5w63BmNX--jv_q0lQrmtF-s00K7lWCTZKzCH0Pc3B_fXN3Fg7Bu8q_b4r0P2nwWYdVsJq7gBKbs-Eb" },
       /*Live*/        { "clientSecret", "EOate6BAQLb7Aj-GK4TfFgKMrqDWqY4KNU2bjGqwn-BzXRTeMVN8Wdud_Xblonwdpqjd1c7PJWtGuwok" },
      /*Live */  {"mode", "live"}
            
               
            };
        }

        // Create accessToken
        private static string GetAccessToken()
        {
            // ###AccessToken
            // Retrieve the access token from OAuthTokenCredential by passing in
            // ClientID and ClientSecret
            // It is not mandatory to generate Access Token on a per call basis.
            // Typically the access token can be generated once and reused within the expiry window      
           
            string accessToken = new OAuthTokenCredential
                (ClientId, ClientSecret, GetConfig()).GetAccessToken();
            return accessToken;
        }

        // Returns APIContext object
        public static APIContext GetAPIContext(string accessToken = "")
        {
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = new APIContext(string.IsNullOrEmpty(accessToken) ?
                GetAccessToken() : accessToken);
            apiContext.Config = GetConfig();

            return apiContext;
        }
    }
}