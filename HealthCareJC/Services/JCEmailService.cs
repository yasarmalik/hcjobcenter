﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using EASendMail;
using System.Net;
using System.Net.Mail;
namespace HealthCareJC.Services
{
    public class JCEmailService
    {
        public void SendEmailToSubscriber(string receiverEmail, string receiverName)
        {

            // var smtp = new System.Net.Mail.SmtpClient("css@healthcarejsc.com", 25); // for healthcare800 domain
            //  var smtp = new System.Net.Mail.SmtpClient("CS@HEALTHCAREJC.COM", 25);

            string subject = "HealthCare Job Center";
            //subscriberEmailContent.txt
            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/subscriberEmailContent.txt"));

            string messageBody = text.Replace("PUTNAMEHERE", receiverName);
            string dateOfSubscription = DateTime.Now.ToString(@"MM\/dd\/yyyy");
            messageBody = messageBody.Replace("PUTDATEHERE", dateOfSubscription);


            // sendMail(messageBody, subject, receiverEmail);
            sendMailFree(messageBody, subject, receiverEmail);

        }
        public Boolean sendMailFree(String message_txt, String Subject, String email)
        {
            /* string mymailAddress = "CS@HEALTHCAREJC.COM";
             string mailPassword = "services";

             SmtpClient smtp = new SmtpClient("smtpout.secureserver.net");
           //SmtpClient smtp = new SmtpClient("smtpout.asia.secureserver.net");// for asia
            // SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            // smtp.EnableSsl = true;
             smtp.Port = 3535;
             smtp.Credentials = new   NetworkCredential(mymailAddress,mailPassword);

             MailMessage message = new MailMessage();
             message.Sender = new MailAddress(mymailAddress,
                   "Health Care Job Center");
                     message.From = new MailAddress(mymailAddress,  "Health Care Job Center");

 message.To.Add(new MailAddress(email,
    "Recipient Number 1"));


 message.Subject = Subject;
 message.Body = message_txt;

 message.IsBodyHtml = true;
 smtp.Send(message);*/
            return true;
        }

        /*  public Boolean sendMail(String message, String Subject, String email)
          {
              try
              {
              EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
              EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();

              // Set sender email address, please change it to yours
             // oMail.From = "testingideofuzion1@gmail.com";
              oMail.From = "CS@HEALTHCAREJC.COM";

              // Set recipient email address, please change it to yours
              oMail.To = email;

              // Set email subject
              oMail.Subject = Subject;

              //message="";
              // Set email body
              oMail.HtmlBody = message;

              // Your SMTP server address
              //SmtpServer oServer = new SmtpServer("smtp.gmail.com");

              SmtpServer oServer = new SmtpServer("smtpout.secureserver.net");
              // User and password for ESMTP authentication, if your server doesn't require
              // User authentication, please remove the following codes.
              oServer.User = "CS@HEALTHCAREJC.COM";
              oServer.Password = "services";

              // If your smtp server requires TLS connection, please add this line
              //oServer.ConnectType = SmtpConnectType.ConnectDirectSSL;
              // If your smtp server requires implicit SSL connection on 465 port, please add this line
            //  oServer.Port = 587;
              oServer.Port = 3535;
             // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;



                oSmtp.SendMail(oServer, oMail);

              }
              catch (Exception ep)
              {
                  throw ep;
                  return false;
              }
              return true;
          }*/
        public void EmailPasswordToNewUser(string receiverEmail, string receiverName, string link, string password)
        {
            string subject = "HealthCare Job Center";

            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/NewPasswordEmailContent.txt"));

            string messageBody = text.Replace("RECIEVEREMAIL", receiverEmail);
            messageBody = messageBody.Replace("RECIEVERNAME", receiverName);

            messageBody = messageBody.Replace("USERPASSWORD", password);

            sendMailFree(messageBody, subject, receiverEmail);


        }
        public void EmailPasswordToNewUserEvent(string receiverEmail, string receiverName, string link, string password)
        {
            string subject = "HealthCare Job Center";

            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/NewPasswordEmailContentEvent.txt"));

            string messageBody = text.Replace("RECIEVEREMAIL", receiverEmail);
            messageBody = messageBody.Replace("RECIEVERNAME", receiverName);

            messageBody = messageBody.Replace("USERPASSWORD", password);

            sendMailFree(messageBody, subject, receiverEmail);


        }


        public void JobPostedEmail(string receiverEmail, string receiverName, string link)
        {
            string subject = "HealthCare Job Center";

            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/JobPostedEmail.txt"));

            string messageBody = text.Replace("RECIEVEREMAIL", receiverEmail);

            messageBody = messageBody.Replace("RECIEVERNAME", receiverName);

            sendMailFree(messageBody, subject, receiverEmail);


        }

        public void EventPostedEmail(string receiverEmail, string receiverName, string link)
        {
            string subject = "HealthCare Job Center";

            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/EventPostedEmail.txt"));

            string messageBody = text.Replace("RECIEVEREMAIL", receiverEmail);

            messageBody = messageBody.Replace("RECIEVERNAME", receiverName);

            sendMailFree(messageBody, subject, receiverEmail);


        }


        public void SendEmailToAdmin(string employerName, string employerEmail)
        {

            //  var smtp = new System.Net.Mail.SmtpClient("smtpout.secureserver.net", 3535); // for healthcare800 domain

            string adminEmail = "CS@HEALTHCAREJC.COM";
            string techEmail = "malik.yasar@gmail.com";
            //  var smtp = new System.Net.Mail.SmtpClient("CS@HEALTHCAREJC.COM", 25);
            string subject = "New Job Posted";
            string messageBody = "\n\nDear Admin" +
                "\n A New Job has Posted Successfully \n\n " + "<br/>" +
                                 "\n\n Username : " + employerName + "<br/>" +
                                "\n Email :\t " + employerEmail + "<br/>" +
                                 "\n\nRegards - HealthCare Job Center";
            //   var mail = new System.Net.Mail.MailMessage();
            //   mail.IsBodyHtml = true;
            //  mail.From = new System.Net.Mail.MailAddress("CS@HEALTHCAREJC.COM", "HealthCare Job Center");

            sendMailFree(messageBody, subject, adminEmail);
            sendMailFree(messageBody, subject, techEmail);

            //smtp.Send("ymalik@hcs800.com", adminEmail, subject, messageBody); // here you can user you name too instead of ymalik@healthcare800.com


        }

        public void SendEmailToAdminEvent(string employerName, string employerEmail)
        {

            //  var smtp = new System.Net.Mail.SmtpClient("smtpout.secureserver.net", 3535); // for healthcare800 domain

            string adminEmail = "CS@HEALTHCAREJC.COM";
            string techEmail = "malik.yasar@gmail.com";
            //  var smtp = new System.Net.Mail.SmtpClient("CS@HEALTHCAREJC.COM", 25);
            string subject = "New Event Posted";
            string messageBody = "\n\nDear Admin" +
                "\n A New Event has Posted Successfully \n\n " + "<br/>" +
                                 "\n\n Username : " + employerName + "<br/>" +
                                "\n Email :\t " + employerEmail + "<br/>" +
                                 "\n\nRegards - HealthCare Job Center";
            //   var mail = new System.Net.Mail.MailMessage();
            //   mail.IsBodyHtml = true;
            //  mail.From = new System.Net.Mail.MailAddress("CS@HEALTHCAREJC.COM", "HealthCare Job Center");

            sendMailFree(messageBody, subject, adminEmail);
            sendMailFree(messageBody, subject, techEmail);

            

        }
    }
}