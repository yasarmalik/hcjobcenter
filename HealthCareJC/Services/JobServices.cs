﻿using HealthCareJC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareJC.Services
{
    public class JobServices
    {
        static ApplicationDbContext _dbContext = new ApplicationDbContext();
        public IEnumerable<SelectListItem> JobTypeData()
        {
            List<SelectListItem> Jobtype = new List<SelectListItem>();
            Jobtype.Add(new SelectListItem() { Text = "Full-Time", Value = "Full-Time" });
            Jobtype.Add(new SelectListItem() { Text = "Part-Time", Value = "Part-Time" });
            Jobtype.Add(new SelectListItem() { Text = "Freelance", Value = "Freelance" });
            Jobtype.Add(new SelectListItem() { Text = "Temporary", Value = "Temporary" });
            return Jobtype;

        }
        public IEnumerable<JobCategory> GetCatagories()
        {
            return _dbContext.jobcategories.ToList();
        }

        public IEnumerable<Speciality> Getspecialities()
        {
            return _dbContext.specialities.ToList();
        }
        public IEnumerable<SelectListItem> CompensationTypeData()
        {
            List<SelectListItem> CompensationType = new List<SelectListItem>();
            CompensationType.Add(new SelectListItem() { Text = "Hour", Value = "Hour" });
            CompensationType.Add(new SelectListItem() { Text = "Month", Value = "Month" });
            CompensationType.Add(new SelectListItem() { Text = "Year", Value = "Year" });
            return CompensationType;

        }

        public IEnumerable<SelectListItem> HealthProfessionalType()
        {
            List<SelectListItem> HealthProfessionalType = new List<SelectListItem>();
            HealthProfessionalType.Add(new SelectListItem() { Text = "MD,NP,PA", Value = "MD,NP,PA" });
            HealthProfessionalType.Add(new SelectListItem() { Text = "Nurses, Med Asst", Value = "Nurses, Med Asst" });
            HealthProfessionalType.Add(new SelectListItem() { Text = "Techs/IT", Value = "Techs/IT" });
            HealthProfessionalType.Add(new SelectListItem() { Text = "Admin Staff", Value = "Admin Staff" });
            HealthProfessionalType.Add(new SelectListItem() { Text = "Others", Value = "Others" });

            return HealthProfessionalType;

        }
        public IEnumerable<SelectListItem> ExperienceLevelData()
        {
            List<SelectListItem> ExperienceLevel = new List<SelectListItem>();
            ExperienceLevel.Add(new SelectListItem() { Text = "1-3", Value = "1-3" });
            ExperienceLevel.Add(new SelectListItem() { Text = "4-7", Value = "4-7" });
            ExperienceLevel.Add(new SelectListItem() { Text = "8-15", Value = "15-30" });
            ExperienceLevel.Add(new SelectListItem() { Text = "30-Above", Value = "30-Above" });
            return ExperienceLevel;
        }
        public IEnumerable<SelectListItem> JobStatusData()
        {
            List<SelectListItem> JobStatus = new List<SelectListItem>();
            JobStatus.Add(new SelectListItem() { Text = "Live", Value = "Live" });
            JobStatus.Add(new SelectListItem() { Text = "Cancel", Value = "Cancel" });
            JobStatus.Add(new SelectListItem() { Text = "Expired", Value = "Expired" });
            return JobStatus;
        }
        public IEnumerable<SelectListItem> EducationLevel()
        {
            List<SelectListItem> JobStatus = new List<SelectListItem>();
            JobStatus.Add(new SelectListItem() { Text = "High School", Value = "High School" });
            JobStatus.Add(new SelectListItem() { Text = "2 Years Completion", Value = "2 Years Completion" });
            JobStatus.Add(new SelectListItem() { Text = "4 Years Completion", Value = "4 Years Completion" });
            JobStatus.Add(new SelectListItem() { Text = "Post Graduation Master", Value = "Post Graduation Master" });
            JobStatus.Add(new SelectListItem() { Text = "Past Graduation Doctorate", Value = "Past Graduation Doctorate" });
            JobStatus.Add(new SelectListItem() { Text = "Post Doctorate", Value = "Post Doctorate" });
            JobStatus.Add(new SelectListItem() { Text = "Internship", Value = "Internship" });
            JobStatus.Add(new SelectListItem() { Text = "Residency", Value = "Residency" });
            JobStatus.Add(new SelectListItem() { Text = "Fellowship", Value = "Fellowship" });
            JobStatus.Add(new SelectListItem() { Text = "Others", Value = "Others" });
            return JobStatus;
        }
    }
}